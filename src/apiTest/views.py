from django.shortcuts import render

# Create your views here.
from django.shortcuts import render

from django.http import HttpResponse
import requests

from . import forms
import json


def index(request):
    # return HttpResponse("トップページなう")
    if request.method == 'GET':
        form = forms.PostNumberForm()
        return render(request, 'apiTest/index.html', {'form': form})
    else:
        address, zipcode = use_api(request.POST.get('num'))
        form = forms.PostNumberForm()
        return render(request, 'apiTest/index.html', {'form': form, 'address': address, 'zipcode': zipcode})


def use_api(num):
    # http: // zipcloud.ibsnet.co.jp / doc / api
    url = "https://zipcloud.ibsnet.co.jp/api/search?zipcode="
    res = requests.get(url + num)
    try:
        text = json.loads(res.text)['results'][0]
        address = text['address1'] + text['address2'] + text['address3']
        zipcode = text['zipcode']
        return address, zipcode
    except:
        return '住所がわかりませんでした', num
